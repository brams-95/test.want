import React, {Component} from 'react';
import {
  TouchableOpacity,
  StyleSheet,
  Text,
  View,
  ActivityIndicator,
  Button,
} from 'react-native';
import AddModal from './Addmodal';

export default class Inicio extends Component {
  constructor(props) {
    super(props);
    this.state = {
      titulo: '',
      date: '',
      showMe: true,
    };
    this.upModal = this.upModal.bind(this);
    this.fetchData = this.fetchData.bind(this);
  }
  fetchData() {
    fetch('https://want.cl/dev/abastible/wp-json/wp/v2/posts/10500', {
      method: 'GET',
      header: {
        Accept: 'application/json',
        'Content-type': 'application/json',
      },
    })
      .then((response) => response.json())
      .then((responseJson) => {
        this.setState({
          showMe: false,
          titulo: responseJson.title.rendered,
          date: responseJson.date_gmt,
        });
      })
      .catch((error) => {
        this.setState({
          showMe: false,
        });
        alert(error);
      });
  }
  upModal() {
    this.refs.addModal.showModal();
  }

  componentDidMount(): void {
    this.fetchData();
  }

  render() {
    return (
      <View style={styles.body}>
        <View style={styles.subcontainer}>
          <TouchableOpacity
            style={styles.button}
            onPress={() => this.props.navigation.navigate('Login')}>
            <Text style={styles.btn_text}>Cerrar sesión</Text>
          </TouchableOpacity>
          {this.state.showMe ? (
            <ActivityIndicator style={styles.viewContent} color="#fff000" />
          ) : (
            <Button title="ver" onPress={this.upModal} />
          )}
        </View>
        <AddModal
          ref={'addModal'}
          titulo={this.state.titulo}
          date={this.state.date}
        />
      </View>
    );
  }
}
const styles = StyleSheet.create({
  subcontainer: {
    width: '100%',
    height: 500,
    flexDirection: 'column',
    justifyContent: 'flex-start',
    alignItems: 'center',
    flex: 1,
  },
  button: {
    backgroundColor: 'white',
    width: 80,
    height: 25,
    justifyContent: 'center',
    alignContent: 'center',
    borderRadius: 25,
    position: 'absolute',
    top: 25,
    right: 25,
  },
  btn_text: {
    fontSize: 10,
    fontWeight: '700',
    color: '#040404',
    alignSelf: 'center',
  },
  title: {
    color: 'white',
  },
  viewContent: {
    display: 'flex',
    marginTop: 80,
  },
  body: {
    backgroundColor: '#2c3e50',
    flex: 1,
    flexDirection: 'column',
    height: '100%',
  },
});
