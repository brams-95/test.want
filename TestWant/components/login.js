import React from 'react';
import {
  StyleSheet,
  Text,
  TextInput,
  TouchableHighlight,
  View,
} from 'react-native';
import AppAsyncStorage from './common/AppAsyncStorage';

export default class Login extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      correo: '',
      contraseña: '',
      iniciado: false,
    };
    this.submit = this.submit.bind(this);
  }
  async componentDidMount(): void {
    /*
    let token = await AppAsyncStorage.retrieveData('token');
    console.log(token);
    token
      ? this.props.navigation.navigate('Inicio')
        : '';*/
  }
  submit() {
    const {correo, contraseña} = this.state;
    if (correo === '' || contraseña === '') {
      if (contraseña === '') {
        alert('se debe llenar el campo de contraseña');
      }
      if (correo === '') {
        alert('Se debe llenar el campo de correo ');
      }
    } else {
      fetch('http://want.cl/dev/consultarCredenciales.php', {
        method: 'POST',
        header: {
          Accept: 'application/json',
          'Content-type': 'application/json',
        },
        body: JSON.stringify({
          correo: correo,
          contraseña: contraseña,
        }),
      })
        .then((response) => response.json())
        .then((responseJson) => {
          if (responseJson.respuesta === 'Credenciales correctas') {
            let token = responseJson.token;
            this.props.navigation.navigate('Inicio');
            /*
            AppAsyncStorage.storeData('token', token)
              ?
              : alert('houston tenemos un problema');*/
          } else {
            this.pass.clear();
            this.correo.clear();
            this.correo.focus();
            alert(responseJson.respuesta);
          }
        })
        .catch((error) => {
          alert(error);
        });
    }
  }
  render() {
    return (
      <View style={styles.container}>
        <Text style={styles.titulo}> Iniciar sesión</Text>
        <TextInput
          style={styles.inputText}
          placeholder="Ingresar email"
          ref={(correo) => {
            this.correo = correo;
          }}
          onChangeText={(correo) => this.setState({correo})}
        />
        <TextInput
          style={styles.inputText}
          placeholder="Ingresar contraseña"
          ref={(contraseña) => {
            this.pass = contraseña;
          }}
          onChangeText={(contraseña) => this.setState({contraseña})}
          textContentType={'password'}
          secureTextEntry={true}
        />
        <TouchableHighlight style={styles.buttonDone} onPress={this.submit}>
          <Text style={styles.textDone}> Ingresar </Text>
        </TouchableHighlight>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  textDone: {
    fontSize: 25,
    fontWeight: 'bold',
    color: 'white',
    marginTop: 4,
    alignSelf: 'center',
  },
  buttonDone: {
    alignSelf: 'center',
    backgroundColor: '#2b42ff',
    width: 150,
    height: 45,
    marginVertical: 30,
    margin: 'auto',
  },
  titulo: {
    fontSize: 40,
    fontWeight: '700',
    color: '#2c3e50',
    marginTop: 25,
  },
  inputText: {
    borderWidth: 1,
    borderColor: 'black',
    padding: 5,
    marginVertical: 20,
    width: 200,
  },
  container: {
    flex: 1,
    backgroundColor: '#fff',
    alignItems: 'center',
    justifyContent: 'center',
  },
});
