import React, {Component} from 'react';
import {
  TouchableOpacity,
  StyleSheet,
  Text,
  View,
  ActivityIndicator,
} from 'react-native';
import Modal from 'react-native-modalbox';

export default class AddModal extends Component {
  constructor(props) {
    super(props);
    this.showModal = this.showModal.bind(this);
    this.closeModal = this.closeModal.bind(this);
  }
  showModal() {
    this.refs.myModal.open();
  }
  closeModal() {
    this.refs.myModal.close();
  }
  render() {
    return (
      <Modal
        style={{width: 250, height: 200}}
        ref={'myModal'}
        position="center"
        backdrop={true}>
        <View>
          <TouchableOpacity
            style={{width: 25, height: 20}}
            onPress={this.closeModal}>
            <Text>X</Text>
          </TouchableOpacity>
          <Text>{this.props.titulo}</Text>
          <Text>{this.props.date}</Text>
        </View>
      </Modal>
    );
  }
}
